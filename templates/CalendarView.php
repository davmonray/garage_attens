<?php
require_once '../../vendor/autoload.php';
use App\calendar\Calendar;

isset($conn) ? "" : require '../../libs/BDD/__connect.php';

    //Find client
if (isset($_GET['client']) && !empty($_GET['client'])){
    $idClient = $_GET['client'];
    $client = $conn->prepare('SELECT * FROM customer WHERE id=:id');
    $client->execute([
       'id' => $_GET['client']
    ]);
    $client = $client->fetch();
}

    //Create and update inter
if (isset($_POST['dateDebutValid']) && !empty($_POST['dateDebutValid'])
&& isset($_POST['dateFinValid']) && !empty($_POST['dateFinValid'])
&& isset($_POST['techId']) && !empty($_POST['techId'])
&& isset($_POST['client']) && !empty($_POST['client'])){
    if(isset($_POST['update']) && !empty($_POST['update']))
    {
        //update
        $update = $conn->prepare('UPDATE intervention SET id_employe = :employee, id_client = :client, date_debut = :debut, date_fin = :fin, description = :description WHERE id=:id');
        $update->execute([
                'id' => $_POST['id_inter'],
                'employee' => $_POST['techId'],
                'client' => $_POST['client'],
                'debut' => str_replace('T', ' ', $_POST['dateDebutValid']),
                'fin' => str_replace('T', ' ', $_POST['dateFinValid']),
                'description' => $_POST['description'],
        ]);
    }else {
        //Create
        $create = $conn->prepare('INSERT INTO intervention (num_inter, id_client, id_employe, date_debut, date_fin, description) VALUES (:ref, :client, :employee, :debut, :fin, :description)');
        $create->execute([
                'ref' => $_POST['reference'],
            'client' => $_POST['client'],
            'employee' => $_POST['techId'],
            'debut' => date("Y-m-d H:i:s", strtotime($_POST['dateDebutValid'])),
            'fin' => date("Y-m-d H:i:s", strtotime($_POST['dateFinValid'])),
            'description' => $_POST['description'],
        ]);
    }
    header('location: ' . $_SERVER['PHP_SELF'] . '?tech=' .$_POST['techId'] . '&week=' . date('W', strtotime($_POST['dateDebutValid'])) . '&client=' . $idClient);
}

    //Liste les employées
$techs = $conn->prepare('SELECT * FROM employe');
$techs->execute();
$techs = $techs->fetchAll();
count($techs) < 1 ? header('location: /admin/index.php'):''; //Verif si il y a des employés dans la bdd
$employee = isset($_GET['tech']) && !empty($_GET['tech']) ? $_GET['tech'] : $techs['0']['id'];

//    On instancie la classe Calendar pour avoir l'objet $calendar
$week = isset($_GET['week']) && !empty($_GET['week']) ? $_GET['week'] : null;
$calendar = new Calendar($week);
$weekDays = $calendar->getWeekDays();

// Suppression inter
if(isset($_GET['delete']) && !empty($_GET['delete'])){
    $delete = $conn->prepare('DELETE FROM intervention WHERE id = :id');
    $delete->execute([
        'id' => $_GET['delete'],
    ]);
    header('location: ' . $_SERVER['PHP_SELF'] . '?tech=' .$_GET['tech'] . '&week=' . $calendar->week);
}

// Si les 2 dates sont identiques on efface tout (ca permet de recommencer au début si on reclic sur le premier choix
if(isset($_POST['dateDebut']) && !empty($_POST['dateDebut']) && isset($_POST['dateFin']) && !empty($_POST['dateFin'])){
    $_POST['dateDebut'] === $_POST['dateFin'] ? header('location: ' . $_SERVER['PHP_SELF'] . '?tech=' .$_POST['tech'] . '&week=' . $calendar->week) : '';
}

//Read des inters
$inters = $conn->prepare('SELECT intervention.id, intervention.id_employe, intervention.id_client, intervention.date_debut, intervention.date_fin, intervention.num_inter, intervention.description, customer.firstname, customer.name FROM intervention INNER JOIN customer ON customer.id=id_client WHERE id_employe = :employee AND date_debut > :debut AND date_fin < :fin');
$inters->execute([
   'employee' => $employee,
    'debut' => date('Y-m-d H:i:s', strtotime(array_key_first(array_flip($weekDays)))),
    'fin' => date('Y-m-d H:i:s', strtotime(array_key_last(array_flip($weekDays)))),
]);
$inters = $inters->fetchAll();

if (isset($_GET['inter']) && !empty($_GET['inter'])){
    $intermodal = $conn->prepare('SELECT intervention.id, intervention.id_employe, intervention.id_client, intervention.date_debut, intervention.date_fin, intervention.num_inter, intervention.description, customer.firstname, customer.name FROM intervention INNER JOIN customer ON customer.id=id_client WHERE intervention.id = :id');
    $intermodal->execute([
       'id' => $_GET['inter'],
    ]);
    $intermodal = $intermodal->fetch();
}

// On veut toutes les intervales des dates de rdv pour l'affichage dans la <table> ensuite
foreach($inters as $inter){
    $intervals[] = $calendar->getInterval($inter['date_debut'], $inter['date_fin']);
}
?>
<?php require 'header.php'; ?>
<!--Titre du haut avec numero de semaine et flèches de sélection-->
<div class="d-flex flex-row align-items-center justify-content-around mx-sm-3 my-3">
    <h1><?= $calendar->toString(); ?></h1>
    <form name="list-employee" class="text-center ml-5" method="GET">
        <h4>Technicien</h4>
        <input type="hidden" name="client" value="<?= isset($_GET['client']) && !empty($_GET['client']) ? $_GET['client']: ''; ?>">
        <?= isset($_GET['visu']) ? '<input type="hidden" name="visu" value="1">' : ''; ?>
        <select name="tech" onChange="this.form.submit()">
            <?php foreach($techs as $tech): ?>
                <option value="<?= $tech['id'] ?>" <?= $employee == $tech['id'] ? 'selected': ''; ?>><?= $tech['prenom'] . ' ' . $tech['nom'] ?></option>
            <?php endforeach; ?>
        </select>
    </form>
    <h2>Semaine <?= $calendar->week ?></h2>
    <div>
        <a href="<?= $_SERVER['PHP_SELF'] ?>?tech=<?= $_GET['tech'] ?? ''; ?>&week=<?= $calendar->previousWeek()->week; ?><?= isset($_GET['client']) && !empty($_GET['client']) ? '&client=' . $_GET['client']: '&visu' ?>" class="btn btn-primary">&lt;</a>
        <a href="<?= $_SERVER['PHP_SELF'] ?>?tech=<?= $_GET['tech'] ?? ''; ?>&week=<?= $calendar->nextWeek()->week; ?><?= isset($_GET['client']) && !empty($_GET['client']) ? '&client=' . $_GET['client']: '&visu' ?>" class="btn btn-primary">&gt;</a>
    </div>
</div>
<table class="calendar__table table table-striped">
<!--    Mise en place des jours de la semaine-->
    <thead>
    <tr>
        <th class="border-none"></th>
    <?php foreach($weekDays as $k => $day): ?>
        <?php if(!strpos($k, 'Lundi') && !strpos($k, 'Dimanche')): ?>
        <th scope="col" class="calendar__up">
                <?= $k ?>
        </th>
        <?php endif; ?>
    <?php endforeach; ?>
    </tr>
    </thead>
<!--    Mise en place des heures avec id du tech et timestamp pour chaque case-->
    <tbody>
        <?php foreach($calendar->hours as $hour => $time): ?>
            <tr>
                <th scope="row" class="text-center border-hour"><?= $hour ?></th>
                    <?php foreach($weekDays as $k => $day): ?>
                        <?php if(!strpos($k, 'Lundi') && !strpos($k, 'Dimanche')): ?>

<!--                        Activation de la case pour la date de début-->
                        <td class="table-block-active <?= isset($_POST['dateDebut']) && $_POST['dateDebut'] == ($day . ' '. $time) ? 'table-block-choice': '';  ?>">

<!--                            Activation de la case si la date est prise-->
                            <?php
                            $test = true;
                            if(isset($intervals)){
                                foreach($intervals as $key => $interval){
                                    if (in_array($day . ' ' . $time, $interval)){
                                        if (isset($idClient) && !empty($idClient)):
                                            echo '<a href="' . $_SERVER['PHP_SELF'] . '?tech=' . $employee . '&week=' . $week . '&inter=' . $inters[$key]['id'] . '&client=' . $idClient . '">';
                                        else :
                                            echo '<a href="' . $_SERVER['PHP_SELF'] . '?tech=' . $employee . '&week=' . $week . '&inter=' . $inters[$key]['id'] . '&visu">';
                                        endif;
                                        if (array_search($day . ' ' . $time, $interval) === 0){echo '<div class="dateDebut">' . $inters[$key]['firstname'] . ' ' . $inters[$key]['name'] . '&nbsp;</div>';}
                                        elseif (array_search($day . ' ' . $time, $interval) === count($interval) - 1){echo '<div class="dateFin">' . substr($inters[$key]['description'], 0, 45) . '</div>';}
                                        else {echo '<div class="dateIntermed">&nbsp;</div>';}
                                        echo '</a>';
                                        $test = false;
                                    }
                                }
                            }?>

                            <?php // On test si la case est libre pour être active à la prise de rdv ?>
                            <?php if(($test) && isset($idClient)): ?>
                            <form action="<?= $_SERVER['PHP_SELF'] . '?tech=' . $employee . '&week=' . $week . '&client=' . $idClient ?>" method="POST">
                                <?php if(isset($_POST['dateDebut']) && !empty($_POST['dateDebut'])): ?>
                                    <input type="hidden" name="dateDebut" value="<?= $_POST['dateDebut'] ?>">
                                    <input type="hidden" name="dateFin" value="<?= $day . ' '. $time ?>">
                                <?php else: ?>
                                    <input type="hidden" name="dateDebut" value="<?= $day . ' '. $time ?>">
                                <?php endif; ?>
                            <input type="hidden" name="tech" value="<?= $employee ?>">
                                <?php if(isset($_POST['dateDebut']) && !empty($_POST['dateDebut'])): ?>
                                    <?= date('YmdHi', strtotime($day . ' '. $time)) >= date('YmdHi', strtotime($_POST['dateDebut'])) ? '<input type="submit" value="" class="btn-cell-calendar">' : '<div class="inactive-case-calendar"></div>' ; ?>
                                <?php else: ?>
                                    <?= date('YmdHi', strtotime($day . ' '. $time)) > date('YmdHi') ? '<input type="submit" value="" class="btn-cell-calendar">' : '<div class="inactive-case-calendar"></div>'; ?>
                                <?php endif; ?>
                            </form>
                            <?php endif; ?>
                        </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<!--Création de la popup lorsque la plage horaire est définie pour rentrer les dernières informations-->
<?php
    if ((isset($_POST['dateDebut']) && !empty($_POST['dateDebut'])
        && isset($_POST['dateFin']) && !empty($_POST['dateFin']))
        || (isset($_GET['inter']) && !empty($_GET['inter']))){
        ?>
        <div class="modal fade" id="modalCalendar" data-show="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content align-items-center">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= isset($_GET['visu']) ? 'Visu' : 'Programmation' ?> de l'intervention<br><br>Technicien: <strong><?= $techs[array_search($employee, array_column($techs, 'id'))]['prenom'] . ' ' . $techs[array_search($employee, array_column($techs, 'id'))]['nom'] ?></strong></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body w-75 text-center">
                        <form action="<?= isset($intermodal) && !empty($intermodal) ? $_SERVER['PHP_SELF'] . '?tech=' . $employee . '&client=' . $intermodal['id_client'] : $_SERVER['PHP_SELF'] . '?tech=' . $employee . '&client=' . $idClient ?>" method="POST">
                            <input type="hidden" name="techId" value="<?= isset($intermodal) ? $intermodal['id_employe'] : $employee ?>">
                            <?php if(isset($intermodal)): ?>
                            <input type="hidden" name="id_inter" value="<?= $intermodal['id'] ?>">
                                <div>
                                    Date de début d'inter:&nbsp;
                                    <?php if(date('YmdHi', strtotime($intermodal['date_debut'])) > date('YmdHi')): ?>
                                        <input type="datetime-local" name="dateDebutValid" value="<?= date('Y-m-d\TH:i', strtotime($intermodal['date_debut'])) ?>" step="1800" required><br><br>
                                    <?php else: ?>
                                    <input type="hidden" name="dateDebutValid" value="<?= date('Y-m-d\TH:i', strtotime($intermodal['date_debut'])) ?>">
                                        <strong><?= date('Y-m-d H:i', strtotime($intermodal['date_debut'])) ?></strong><br><br>
                                    <?php endif; ?>
                                    Date de fin d'inter: <input type="datetime-local" name="dateFinValid" value="<?= date('Y-m-d\TH:i', strtotime($intermodal['date_fin'])) ?>" step="1800" required
                                    <?php if(date('YmdHi', strtotime($intermodal['date_debut']) < date('YmdHi'))): ?>
                                        min="<?= date('Y-m-d\TH:i', strtotime($intermodal['date_debut'])) ?>"
                                    <?php endif; ?>
                                    >
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="dateDebutValid" value="<?= $_POST['dateDebut'] ;?>">
                                <input type="hidden" name="dateFinValid" value="<?= $_POST['dateFin'] ?>">
                                <div>
                                    Date de début d'inter: <strong><?= $_POST['dateDebut'] ?></strong><br>
                                    Date de fin d'inter: <strong><?= $_POST['dateFin'] ?></strong>
                                </div>
                            <?php endif; ?>
                            <br>
                            <table class="m-auto w-100">
                                <tbody class="text-center">
                                <tr class="d-flex flex-column align-items-center">
                                    <td><strong>Nom du client:</strong></td>
                                    <td class="w-100"><input type="hidden" name="client" value="<?= isset($intermodal) ? $intermodal['id_client'] . '">' .  $intermodal['firstname'] . ' ' . $intermodal['name'] : $client['id'] . '">' . $client['firstname'] . ' ' . $client['name'] ?></td>
                                </tr>
                                <tr class="d-flex flex-column align-items-center">
                                    <td><br><strong>Description de l'intervention:</strong></td>
                                    <td class="w-100"><textarea name="description" rows="6" class="w-75"><?= isset($intermodal) ? $intermodal['description']:''; ?></textarea></td>
                                </tr>
                                <tr class="d-flex flex-column align-items-center">
                                    <td><br><strong>Référence à donner au client pour le suivi d'inter</strong></td>
                                    <?php $ref = time(); ?>
                                    <td><br><input type="hidden" name="reference" value="<?= isset($intermodal) ? $intermodal['num_inter'] : $ref ?>"><span class="ref_suivi"><?= isset($intermodal) ? $intermodal['num_inter'] : $ref ?></span></td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                            <?php if (!isset($_GET['visu'])): ?>
                                <?php if(isset($intermodal)): ?>
                                    <input type="submit" class="btn btn-warning btn-large" name="update" value="Mettre à jour"><br><br>
                                    <a href="<?= $_SERVER['PHP_SELF'] . "?tech=" . $_GET['tech'] . "&week=" . $calendar->week . "&delete=" . $intermodal['id'] . '&client=' . $idClient ?>" class="btn btn-outline-danger btn-large">Supprimer la fiche</a>
                                <?php else: ?>
                                    <input type="submit" class="btn btn-success btn-large" value="Enregistrer">
                                <?php endif; ?>
                            <?php endif; ?>
                        </form>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
?>
<br><br><br>


