<?php

namespace App\calendar;

use Exception;

class Calendar {

    public $days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
    private $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    public $hours = [
        '9h' => '09:00:00',
        '9h30' => '09:30:00',
        '10h' => '10:00:00',
        '10h30' => '10:30:00',
        '11h' => '11:00:00',
        '11h30' => '11:30:00',
        '12h' => '12:00:00',
        '12h30' => '12:30:00',
        '13h' => '13:00:00',
        '13h30' => '13:30:00',
        '14h' => '14:00:00',
        '14h30' => '14:30:00',
        '15h' => '15:00:00',
        '15h30' => '15:30:00',
        '16h' => '16:00:00',
        '16h30' => '16:30:00',
        '17h' => '17:00:00',
        '17h30' => '17:30:00',
        '18h' => '18:00:00',
        '18h30' => '18:30:00',
        '19h' => '19:30:00',
        ];
    public $month;
    public $year;
    public $week;
    public $firstDayWeek;
    public $lastDayWeek;

    /**
     * @throws Exception
     */
    public function __construct($week = null, $year = null)
    {
        if ($week === null){
            $week = intval(date('W'));
        }
        if($year === null){
            $year = intval(date('Y'));
        }
        $this->week = $week;
        $this->year = $year;
        $this->date = mktime(0,0,0,1,($week * 7), $year);
        $this->month = date('m', $this->date);
    }

    // renvoi les dates des jours de la semaine demandée
    public function getWeekDays()
    {
        $thisDay = intval(date('w', $this->date));
        $firstDay = new \DateTime(date('Y-m-d', $this->date));
        $firstDay->modify('-'. ($thisDay-1) . ' day');
        $this->firstDayWeek = $firstDay;
        foreach($this->days as $key => $value){
            $active = $this->firstDayWeek->format('Y-m-d') === date('Y-m-d') ? 'calendar_day_active' : '';
            $days['<div class="calendar__weekday ' . $active . '">' . $value . '</div><div class="calendar__day">' . $firstDay->format('d') . ' ' . $this->months[intval($firstDay->format('m'))-1] . '</div>'] = $firstDay->format('Y-m-d');
            $firstDay->modify('+1 day');
        }
        return $days;
    }

    public function toString(): string
    {
        return $this->months[intval($this->month)-1].' '.$this->year ;
    }

    //Renvoi la semaine suivante ou précédente

    public function nextWeek(): Calendar
    {
        $week = $this->week + 1;

        return new Calendar($week);
    }

    public function previousWeek(): Calendar
    {
        $week = $this->week - 1;

        return new Calendar($week);
    }

    // Renvoi un tableau des valeurs intervales entre 2 dates pour un rdv

    public function getInterval($dateDebut, $dateFin)
    {
        $intervals[] = $dateDebut;
        while($dateDebut < $dateFin){
            $dateDebut = date('Y-m-d H:i:s', strtotime(' +30 minutes', strtotime($dateDebut)));
            $intervals[] = $dateDebut;
        }
        return $intervals;
    }
}
