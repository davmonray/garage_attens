<?php
require '../libs/BDD/__connect.php';

// Si il existe déja une connection on redirige vers la page qu'il faut
if(isset($sessionData)){
    header('location: /admin/index.php');
}

// Test de connection et redirection vers /admin si login/mdp correctes
if (isset($_POST['login']) && isset($_POST['password'])
    && !empty($_POST['login']) && !empty($_POST['password'])) {

    $user = $conn->prepare("SELECT * FROM user WHERE username = :username");
    $user->execute([
        'username' => htmlspecialchars($_POST['login']),
    ]);

    if ($user->rowCount() > 0) {
        $user = $user->fetch();

        // vérification du mot de passe
        if ( password_verify($_POST['password'], $user['pwd'])) {
            // on garde en session l'id de l'utilisateur connecté
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['role'] = $user['roles'];
            header('Location: /admin/index.php');
        }else{$authentification = 'failed';}
    }

}
// fin du test

$nav = 'login'; // valeur récupérée dans le header.php pour la surbrillance dans le menu
$title = "connection à l'espace membres";
require './header.php'; // On appel la partie header
?>

<style>body{height:100%}main{display:-ms-flexbox;display:-webkit-box;display:flex;-ms-flex-align:center;-ms-flex-pack:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:center;justify-content:center;padding-top:40px;padding-bottom:40px;background-color:#f5f5f5}.form-signin{width:100%;max-width:330px;padding:15px;margin:0 auto}.form-signin .checkbox{font-weight:400}.form-signin .form-control{position:relative;box-sizing:border-box;height:auto;padding:10px;font-size:16px}.form-signin .form-control:focus{z-index:2}.form-signin input[type=email]{margin-bottom:-1px;border-bottom-right-radius:0;border-bottom-left-radius:0}.form-signin input[type=password]{margin-bottom:10px;border-top-left-radius:0;border-top-right-radius:0}</style>
<main class="text-center">
<form class="form-signin" action="" method="post">
      <img class="mb-4" src="../assets/img/login.png" alt="" width="90" height="90">
      <h1 class="h3 mb-3 font-weight-normal">Espace Staff</h1>
      <?php if(isset($authentification) && $authentification === 'failed'){echo '<span class="text-danger">Login ou Mot de passe invalide';} ?>
      <label for="inputEmail" class="sr-only">Login</label>
      <input type="texte" id="inputEmail" name="login" class="form-control" placeholder="Login" required autofocus>
      <label for="inputPassword" class="sr-only">Mot de passe</label>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Mot de passe" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
    </form>
</main>
<?php require './footer.php';?> <!-- On appel la section footer -->