<?php
$nav = 'index'; // Variable qui sera récupérée dans le header pour mettre le lien de la page en surbillance dans la navbar
require 'header.php'; // On récupère le header
?>
<section class="p-5 section-header">
    <div class="container d-flex align-items-baseline banner-header">
        <div class="text-left p-5">
            <h1 class="text-white">Votre <strong>expert auto</strong><br>depuis plus de <strong>20 ans</strong></h1>
            <h4 class="text-white">Déjà plus de <strong>3500 clients</strong> nous font confiance !</h4>
        </div>
        <div class="img-banner">
            <img src="./assets/img/depann.png" id="img-banner">
        </div>
    </div>
</section>
<div class="container bg-dark p-3 banner-suivi">
    <form action="inters.php" method="POST">
        <input id="num-suivi" type="text" name="inter" placeholder="Mon numéro de suivi">&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" class="btn btn-danger btn-suivi" value="VOIR LES INFOS">
    </form>
</div>
<div class="container reassurance d-flex px-5 my-5 justify-content-around">
    <h5>
        <img src="./assets/img/check.png">&nbsp;<strong>Garantie 6 mois</strong> pièces et main d'oeuvre
    </h5>
    <h5>
        <img src="./assets/img/check.png">&nbsp;<strong>Devis</strong> et <strong>RDV</strong> immédiat
    </h5>
    <h5>
        <img src="./assets/img/check.png">&nbsp;Partez <strong>l'esprit tranquille</strong>
    </h5>
</div>
<hr class="hr-accueil">
<section>
    <div class="bandeau-story container">
        <p>
            <img src="assets/img/logo-dark.png"><br><br>Attens père et chien est votre guichet unique pour tous vos besoins en matière de réparation et personnalisation automobile. Situé à La-Chartre-sur-le-Loir, l'atelier abrite une équipe expérimentée de techniciens, de fabricants, de designers, d'électriciens, de peintres et bien plus encore. Chaque jour, l'équipe travaille dans le but de satisfaire le maximum de clients en leur offrants les meilleurs services.
        </p>
        <div>
            <img src="assets/img/auto-repair.jpg">
        </div>
    </div>
</section>
<section class="bg-fixed">
    <div class="container">
        <h3>Confiez nous votre auto et partez l'esprit tranquille !</h3>
        <section>
            <br>
            <img src="./assets/img/banner.jpg" width="100%">
        </section>
    </div>
</section>
<div class="container">
    <div class="row justify-content-center">
        <div class=" col-sm-11 col-md-9 col-lg-8 col-xl-7">
            <div class="card">
                <p class="post"> <span><img class="quote-img" src="assets/img/quotes.png"></span> <span class="post-txt">Plus de 10 ans que je pionce devant le comptoir et j'ai jamais entendu un client se plaindre </span> <span><img class="nice-img" src="https://i.imgur.com/l5AkSHd.png"></span> </p>
            </div>
            <div class="arrow-down"></div>
            <div class="row d-flex justify-content-center">
                <div class=""> <img class="profile-pic fit-image" src="assets/img/chien.jpg"> </div>
                <p class="profile-name">Just Attens</p>
            </div>
        </div>
    </div>
</div>
<br><br><br>
<?php require 'footer.php';?> <!-- On récupèere le footer -->