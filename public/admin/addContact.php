<?php

require '../../libs/BDD/__connect.php';
$sessionData && ($sessionData['roles'] == 'ROLE_ADMIN') ? "":header('location: /login.php');

    //Update & Create
if(isset($_POST['login']) && !empty($_POST['login'])
        && isset($_POST['password']) && !empty($_POST['password'])){
        if(isset($_POST['id'])){
            $update = $conn->prepare('UPDATE user SET username=:username, pwd=:pwd, roles=:roles WHERE id=:id');
            $update->execute([
               'username' =>  $_POST['login'],
                'pwd' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                'roles' => $_POST['roles'],
                'id' => $_POST['id'],
            ]);
        }else{
            $create = $conn->prepare('INSERT INTO user (username, pwd, roles) VALUE (:username, :pwd, :roles)');
            $create->execute([
               'username' => $_POST['login'],
               'pwd' =>  password_hash($_POST['password'], PASSWORD_DEFAULT),
                'roles' => $_POST['roles'] == 'Gérant' ? 'ROLE_ADMIN': 'ROLE_USER',
            ]);
        }
    header('location: /admin/Contact.php');
}

    //Read
if(isset($_GET['id']) && !empty($_GET['id'])){
    $datas = $conn->prepare('SELECT * FROM user WHERE id = :id');
    $datas->execute([
       'id' => $_GET['id'],
    ]);
    $datas = $datas->fetch();
}

?>
<?php require 'header.php'; ?>
<main class="container">
    <form class="col-5 m-auto" method="POST">
        <?= isset($datas) && !empty($datas) ? '<input type="hidden" value="'. $datas['id'] .'" name="id"':""; ?>
        <div class="form-group">
            <label for="exampleInputEmail1"><strong>Login</strong></label>
            <input type="text" class="form-control" name="login" required="true" id="exampleInputEmail1" <?= isset($datas) && !empty($datas) ? 'value="'. $datas['username'] .'"':"";?> >
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1"><strong>Mot de passe</strong></label>
            <input type="password" class="form-control" name="password" id="exampleInputPassword1" required="true">
        </div>
        <div class="form-group">
            <label for="roles"><strong>Type de compte</strong></label>
            <select class="form-control" name="roles" id="roles">
                <option>Employé(e)</option>
                <option>Gérant</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>
</main>
<?php require '../footer.php';?>

