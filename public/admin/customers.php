<?php
isset($_GET['inter']) && $_GET['inter'] ? $nav = 'inters' : $nav = 'customers';
require_once '../../libs/BDD/__connect.php';
$sessionData && ($sessionData['roles'] == 'ROLE_ADMIN' || $sessionData['roles'] == 'ROLE_USER') ? "":header('location: /login.php');
    // Update des clients
if (isset($_POST['name']) && !empty($_POST['name'])
&& isset($_POST['firstname']) && !empty($_POST['firstname'])
&& isset($_POST['brand']) && !empty($_POST['brand'])
&& isset($_POST['type']) && !empty($_POST['type'])
&& isset($_POST['registration']) && !empty($_POST['registration'])
&& isset($_POST['tel']) && !empty($_POST['tel'])){
        $update = $conn->prepare('UPDATE customer SET name=:name, firstname=:firstname, brand=:brand, type=:type, registration=:registration, tel=:tel, email=:email, comments=:comments WHERE id=:id');
        $update->execute([
            'name' =>  $_POST['name'],
            'firstname' => $_POST['firstname'],
            'brand' => $_POST['brand'],
            'id' => $_POST['id'],
            'type' => $_POST['type'],
            'registration' => $_POST['registration'],
            'tel' => $_POST['tel'],
            'email' => $_POST['email'],
            'comments' => $_POST['comments'],
        ]);
        header('location: customers.php?client=' . $_POST['id']);
}

    // Delete des client
if(isset($_GET['delete']) && !empty($_GET['delete'])){
    $delete = $conn->prepare('DELETE FROM customer WHERE id=:id');
    $delete->execute([
       'id' => $_GET['delete'],
    ]);
    header('location: customers.php');
}

    //Read des clients
if(isset($_GET['client']) && !empty($_GET['client'])){
    $client = $conn->prepare('SELECT * FROM customer WHERE id=:id');
    $client->execute([
       'id' => $_GET['client'],
    ]);
    $client = $client->fetch();
}elseif((isset($_POST['nom']) && !empty($_POST['nom']))
    || (isset($_POST['prenom']) && !empty($_POST['prenom']))
    || (isset($_POST['vehicule']) && !empty($_POST['vehicule']))
    || (isset($_POST['immat'])) && !empty($_POST['immat'])
    || (isset($_POST['typecar'])) && !empty($_POST['typecar'])){
    //Recherche
    $datas = $conn->prepare('SELECT * FROM customer WHERE name=:nom OR firstname=:prenom OR brand=:typecar OR type=:vehicule OR registration=:immat');
    $datas->execute([
        'nom' => $_POST['nom'],
        'prenom' => $_POST['prenom'],
        'typecar' => $_POST['typecar'],
        'vehicule' => $_POST['vehicule'],
        'immat' => $_POST['immat'],
    ]);
    $datas = $datas->fetchAll();
}else{
    $datas = $conn->prepare('SELECT * FROM customer LIMIT 20');
    $datas->execute();
    $datas = $datas->fetchAll();
}

?>
<?php require 'header.php'; ?>
<main class="container">
    <?php if(isset($client)): ?>
        <h1><a href="customers.php" class="btn btn-outline-success"><img src="../assets/img/return.png"></a>&nbsp;&nbsp;Fiche client numéro <?= $client['id'] ?></h1>
        <h3>(<?= $client['name'] . ' ' . $client['firstname'] ?>)</h3><br>
        <form class="d-flex flex-column align-items-center" method="POST" action="customers.php?client=<?= $client['id'] ?>">
            <input type="hidden" name="id" value="<?= $client['id'] ?>">
            <div class="form-group col-md-6">
                <label for="name">Nom</label>
                <input type="text" class="form-control" id="name" name="name" required value="<?= $client['name'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="firstname">Prénom</label>
                <input type="text" class="form-control" id="firstname" name="firstname" required value="<?= $client['firstname'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="marque">Marque du véhicule</label>
                <input type="text" class="form-control" id="marque" name="brand" required value="<?= $client['brand'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="type">Type du véhicule</label>
                <input type="text" class="form-control" id="type" name="type" required value="<?= $client['type'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="immat">Immatriculation</label>
                <input type="text" class="form-control" id="immat" name="registration" required value="<?= $client['registration'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="tel">Téléphone</label>
                <input type="text" class="form-control" id="tel" name="tel" required value="<?= $client['tel'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="<?= $client['email'] ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="comments">Commentaires</label>
                <textarea class="form-control" id="comments" rows="6" name="comments"><?= $client['comments'] ?></textarea>
            </div>
            <div class="form-group col-md-6">
                <input type="submit" class="btn btn-primary" value="Modifier">&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href="customers.php?delete=<?= $client['id'] ?>">Supprimer</a>
            </div>
        </form>
    <?php else: ?>
    <h1>Liste des clients&nbsp;&nbsp;&nbsp;&nbsp;<a href="add_customer.php<?= isset($_GET['inter']) && $_GET['inter'] ? '?prog=true' : '' ?>" class="btn btn-primary">Ajouter un nouveau client</a></h1><br>
    <table class="table table-striped">
        <thead>
        <h5 style="text-align: left">Recherche</h5>
        <form method="POST" action="customers.php">
            <tr>
                <th><input type="text" name="nom" placeholder="nom"></th>
                <th><input type="text" name="prenom" placeholder="prénom"></th>
                <th><input type="text" name="typecar" placeholder="marque véhicule"></th>
                <th><input type="text" name="vehicule" placeholder="type véhicule"></th>
                <th><input type="text" name="immat" placeholder="immatriculation"></th>
                <th><input type="submit" value="Rechercher"></th>
            </tr>
        </form>
        <tr>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Marque véhicule</th>
            <th scope="col">Modèle véhicule</th>
            <th scope="col">Immatriculation</th>
            <th>&nbsp</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($datas as $data): ?>
            <tr>
                <td><?= $data['name'] ?></td>
                <td><?= $data['firstname'] ?></td>
                <td><?= $data['brand'] ?></td>
                <td><?= $data['type'] ?></td>
                <td><?= $data['registration'] ?></td>
                <?php if (isset($_GET['inter']) && $_GET['inter']): ?>
                    <td><a href="addInter.php?client=<?= $data['id'] ?>" class="btn btn-primary">Sélectionner ce client</a></td>
                <?php else: ?>
                    <td><a href="/admin/customers.php?client=<?= $data['id'] ?>" class="btn btn-success">Voir la fiche</a></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
    <br><br><br>
</main>

<?php require  '../footer.php'; ?>
