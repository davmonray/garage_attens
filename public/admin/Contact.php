<?php

require '../../libs/BDD/__connect.php';
$sessionData && ($sessionData['roles'] == 'ROLE_ADMIN') ? "":header('location: /login.php');

//Read
$datas = $conn->prepare('SELECT * FROM user');
$datas->execute();
$datas = $datas->fetchAll();
$nav = 'contact';
?>
<?php require 'header.php'; ?>
    <main class="container">
        <div class="d-flex justify-content-between mb-3">
            <h2>Liste des utilisateurs</h2>
            <a href="addContact.php" class="btn btn-success">Ajouter un nouvel utilisateur</a>
            <p>&nbsp;</p>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Login</th>
                <th scope="col">Role</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($datas as $data){ ?>
                <tr>
                    <th scope="row"><?= $data['id'] ?></th>
                    <td><?= $data['username'] ?></td>
                    <td><?= $data['roles'] ?></td>
                    <td><a href="addContact.php?id=<?= $data['id'] ?>" class="btn btn-primary">Modifier utilisateur</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <br>
    </main>
<?php require '../footer.php';?><?php
