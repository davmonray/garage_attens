<?php

// Test pour savoir si un utilisateur est connecté
// On redirige vers la page login si aucun ne l'est

require_once '../../libs/BDD/__connect.php';
$sessionData && ($sessionData['roles'] == 'ROLE_ADMIN' || $sessionData['roles'] == 'ROLE_USER') ? "":header('location: /login.php');

//Read
$data = null;
$error = null;
if(isset($_GET['id']) && !empty($_GET['id'])){
    $data = $conn->prepare('SELECT * from employe WHERE id=:id');
    $data->execute([
        'id' => $_GET['id'],
    ]);
    $data = $data->fetch();
    $user = $conn->prepare('SELECT * FROM user WHERE id=:username');
    $user->execute([
       'username' => $data['id_user'],
    ]);
    $user = $user->fetch();

    // suppression d'un User
    if (isset($_GET['supp']) && $_GET['supp'] && $user){
        $delete = $conn->prepare('DELETE FROM user WHERE id=:id');
        $delete->execute([
           'id' => $user['id'],
        ]);
        $delete = $conn->prepare('UPDATE employe SET id_user=NULL WHERE id=:id');
        $delete->execute([
           'id' => $_GET['id'],
        ]);
        header('location: donnees_employe.php?id=' . $_GET['id']);
    }
}
$id=null;
//Create for login & mot de passe
if(isset($_GET['id']) && !empty($_GET['id'])
    && isset($_POST['login']) && !empty($_POST['login'])
    && isset($_POST['password']) && !empty($_POST['password'])){
    $test = $conn->prepare('SELECT * FROM user WHERE username=:login');
    $test->execute([
       'login' => $_POST['login'],
    ]);
    if($test->rowCount() < 1){
        $create = $conn->prepare('INSERT INTO user(username, pwd, roles) VALUES (:login, :password, :roles)');
        $create->execute([
            'login' => $_POST['login'],
            'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
            'roles' => $_POST['roles'] == 'Gérant' ? 'ROLE_ADMIN': 'ROLE_USER',
        ]);
        $id = $conn->prepare('SELECT * FROM user WHERE username=:login');
        $id->execute([
            'login' => $_POST['login'],
        ]);
        $id = $id->fetch();
        $employe = $conn->prepare('UPDATE employe SET id_user=:id_user WHERE id=:id');
        $employe->execute([
           'id_user' => $id['id'],
            'id' => $_GET['id'],
        ]);
        header('location: donnees_employe.php?id=' . $_GET['id']);
    }else{
        $error = 1;
    }
}
// Fin du test
//Exécution de la requête pour l'enregistrement des données dans la table employé
    if (isset($_POST['prenom'])
        && !empty($_POST['prenom'])
        && isset($_POST['nom'])
        && !empty($_POST['nom'])
        && isset($_POST['adresse'])
        && !empty($_POST['adresse'])
        && isset($_POST['CP'])
        && !empty($_POST['CP'])
        && isset($_POST['ville'])
        && !empty($_POST['ville'])
        && isset($_POST['telephone'])
        && !empty($_POST['telephone'])
        && isset($_POST['mail'])
        && isset($_POST['num_secu'])
        && !empty($_POST['num_secu'])
        && isset($_POST['date_entree'])) {
        if(isset($_POST['id']) && !empty($_POST['id'])){
            //Update
            $update = $conn->prepare('UPDATE employe SET prenom=:prenom, nom=:nom, adresse=:adresse, CP=:cp, ville=:ville, telephone=:tel, mail=:mail, num_secu=:secu, date_entree = :entree, date_sortie=:depart WHERE id=:id');
            $update->execute([
               'prenom' => $_POST['prenom'],
               'nom' => $_POST['nom'],
               'adresse' => $_POST['adresse'],
               'cp' => $_POST['CP'],
               'ville' => $_POST['ville'],
               'tel' => $_POST['telephone'],
               'mail' => $_POST['mail'],
               'secu' => $_POST['num_secu'],
               'entree' => date('Y-m-d H:i:s', strtotime($_POST['date_entree'])),
               'depart' => $_POST['date_depart'] ? date('Y-m-d H:i:s', strtotime($_POST['date_depart'])) : NULL,
                'id' => $_POST['id'],
            ]);
            header('location: donnees_employe.php?id=' . $_POST['id']);
        }else{
            //Create
            $create = $conn->prepare("INSERT INTO `employe` (`prenom`, `nom`, `adresse`, `CP`, `ville`, `telephone`, `mail`, `num_secu`, `date_entree`, id_user) VALUES (:prenom, :nom, :adresse, :CP, :ville, :telephone, :mail, :num_secu, :date_entree, :id)");
            $create->execute([
                'prenom' => $_POST['prenom'],
                'nom' => $_POST['nom'],
                'adresse' => $_POST['adresse'],
                'CP' => $_POST['CP'],
                'ville' => $_POST['ville'],
                'telephone' => $_POST['telephone'],
                'mail' => $_POST['mail'],
                'num_secu' => $_POST['num_secu'],
                'date_entree' => date('Y-m-d H:i:s', strtotime($_POST['date_entree'])),
                'id' => $id['id'] ?? null,
            ]);
            header('Location: /admin/staff.php');
        }
    }
?>
<?php require 'header.php'; ?>
<main class="container">
<!--Formulaire permettant la création d'un nouvel employé dans La base de données-->
    <?= isset($_GET['id']) && !empty($_GET['id']) ? '<h1>Modification de fiche</h1>' : '<h1>Création fiche employé</h1>' ?><br><br>
    <?= $data && is_null($data['id_user']) ? '<div class="text-danger">Login et mot de passe non défini pour cette utilisateur</div>' : '' ?>
    <?= $error === 1 ? '<div class="text-danger">Login déjà utilisé, veuillez en choisir un autre</div>' : ''; ?>
    <div class="tab-employe">
        <div class="col-md-6">
            <form method="POST" class="col-10 m-auto bg-flight rounded" action="donnees_employe.php">
            <div class="form-group">
                <label for="prenom">Prénom</label>
                <input type="text" class="form-control" id="prenom" name="prenom" value="<?= $data['prenom'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" id="nom" name="nom" value="<?= $data['nom'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="adresse">Adresse</label>
                <input type="text" class="form-control" id="adresse" name="adresse" value="<?= $data['adresse'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="CP">Code Postal</label>
                <input type="text" class="form-control" id="CP" name="CP" value="<?= $data['CP'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="ville">Ville</label>
                <input type="text" class="form-control" id="ville" name="ville" value="<?= $data['ville'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="telephone">Téléphone</label>
                <input type="text" class="form-control" id="telephone" name="telephone" value="<?= $data['telephone'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="mail">Mail</label>
                <input type="email" class="form-control" id="mail" name="mail" value="<?= $data['mail'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="num_secu">N° secu</label>
                <input type="text" class="form-control" id="num_secu" name="num_secu" value="<?= $data['num_secu'] ?? ''; ?>">
            </div>
            <div class="form-group">
                <label for="entree">date d'entrée</label>
                <input type="date" class="form-control" id="entree" name="date_entree" value="<?= $data['date_entree'] ?? ''; ?>">
            </div>
                <div class="form-group">
                <label for="depart" class="text-danger">date de départ de l'entreprise</label>
                <input type="date" class="form-control" id="depart" name="date_depart" value="<?= $data['date_sortie'] ?? ''; ?>">
            </div>
                <input type="hidden" name="id" value="<?= $data['id'] ?? ''; ?>">
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </form>
        </div>
        <div class="col-md-4">
            <form method="POST" class="col-10 m-auto bg-info rounded" action="donnees_employe.php?id=<?= $data ? $data['id'] : ""; ?>">
                <br>
            <div class="form-group">
                <label for="exampleInputEmail1"><strong>Login</strong></label>
                <input type="text" class="form-control" name="login" required="true" id="exampleInputEmail1" value="<?= $user['username'] ?? '';?>" >
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1"><strong>Mot de passe</strong></label>
                <input type="password" class="form-control" name="password" id="exampleInputPassword1" required="true">
            </div>
            <div class="form-group">
                <label for="roles"><strong>Type de compte</strong></label>
                <select class="form-control" name="roles" id="roles">
                    <option>Employé(e)</option>
                    <option>Gérant</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
                <?php if(isset($user)): ?>
                    <br><br><a href="donnees_employe.php?id=<?= $data['id'] ?>&supp=true" class="btn btn-danger">Supprimer l'accès</a>
                <?php endif ?>
                <br><br>
            </form>
        </div>
    </div>
    <br><br>
</main>

<?php require '../footer.php';?>