<?php
session_start();
session_destroy(); // On libère la variable de session
header('location: /'); // On redirige vers la page d'accueil du site