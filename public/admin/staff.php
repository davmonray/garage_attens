<?php

// Test pour savoir si un utilisateur est connecté
// On redirige vers la page login si aucun ne l'est

require '../../libs/BDD/__connect.php';
$sessionData && ($sessionData['roles'] == 'ROLE_ADMIN' || $sessionData['roles'] == 'ROLE_USER') ? "":header('location: /login.php');
$user = $conn->prepare('SELECT * FROM `employe` INNER JOIN user ON employe.id_user = user.id WHERE user.username = :username');
$user->execute([
    'username' => $sessionData['username'],
]);
$user = $user->fetch();
// Fin du test

//Exécution de la requête pour l'affichage de la liste des employés

// On prépare la requête pour la liste des employés toujours en poste
$datas_actu = $conn->prepare('SELECT * FROM `employe` WHERE isnull(date_sortie)');
$datas_actu->execute();
$data_actu = $datas_actu->fetchAll();

// On prepare la requête pour la liste des employés passés
$datas_past = $conn->prepare('SELECT * FROM employe WHERE date_sortie IS NOT NULL');
$datas_past->execute();
$datas_past = $datas_past->fetchAll();

// On stocke le résultat dans un tableau associatif


?>
<?php $nav = "staff"; require 'header.php'; ?>
<main class="container">
    <?php if($sessionData['roles'] === 'ROLE_ADMIN') : ?>

        <div class="d-flex justify-content-between mb-3">
            <h1>Liste des employés</h1>
            <a href="donnees_employe.php" class="btn btn-success px-4">&nbsp;&nbsp;&nbsp;Créer&nbsp;&nbsp;&nbsp;</a>
            <p>&nbsp;</p>
        </div>
        <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Email</th>
                <th scope="col">Accès</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data_actu as $employe){?>
                <tr>
                    <td>
                        <?= $employe['nom'] ?>
                    </td>
                    <td>
                        <?= $employe['prenom'] ?>
                    </td>
                    <td>
                        <?= $employe['mail'] ?>
                    </td>
                    <td>
                        <?= $employe['id_user'] ? '<img src="../assets/img/check.png">' : ''; ?>
                    </td>
                    <td><a href="donnees_employe.php?id=<?= $employe['id'] ?>" class="btn btn-primary">Consulter</a></td>
                </tr>
            <?php } ?>
            <?php foreach($datas_past as $past){?>
                <tr style="background-color: lightgray">
                    <td>
                        <?= $past['nom'] ?>
                    </td>
                    <td>
                        <?= $past['prenom'] ?>
                    </td>
                    <td>
                        <?= $past['mail'] ?>
                    </td>
                    <td>
                        <?= $past['id_user'] ? '<img src="../assets/img/check.png">' : ''; ?>
                    </td>
                    <td><a href="donnees_employe.php?id=<?= $employe['id'] ?>" class="btn btn-primary">Consulter</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php elseif($user): ?>
        <h2>Vos données nous concernant</h2>
        <p>(Si vous constatez la moindre erreur, veuillez en tenir informer la direction)</p>
        <br>
    <table width="100%">
        <tr>
            <td align="right" width="50%"><strong>Nom:</strong></td>
            <td align="left" width="50%">&nbsp;<?= $user['nom'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Prenom:</strong></td>
            <td align="left">&nbsp;<?= $user['prenom'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>adresse:</strong></td>
            <td align="left">&nbsp;<?= $user['adresse'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Code postal:</strong></td>
            <td align="left">&nbsp;<?= $user['CP'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Ville:</strong></td>
            <td align="left">&nbsp;<?= $user['ville'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Téléphone:</strong></td>
            <td align="left">&nbsp;<?= $user['telephone'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Mail:</strong></td>
            <td align="left">&nbsp;<?= $user['mail'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Numéro de sécurité sociale:</strong></td>
            <td align="left">&nbsp;<?= $user['num_secu'] ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Date d'entrée dans la société:</strong></td>
            <td align="left">&nbsp;<?= $user['date_entree'] ?></td>
        </tr>
    </table>
    <?php else: ?>
    <p class="text-danger text-center">Votre utilisateur n'est rattaché à aucun employé, veuillez contacter la direction</p>
    <?php endif ?>
    <br>
</main>
<?php require '../footer.php';?>