<?php

// Test pour savoir si un utilisateur est connecté
// On redirige vers la page login si aucun ne l'est
require '../../libs/BDD/__connect.php';

$sessionData && ($sessionData['roles'] == 'ROLE_ADMIN' || $sessionData['roles'] == 'ROLE_USER') ? "":header('location: /login.php');
$nav = 'inters';
$css = '../assets/calendar.css';

// on enregistre la date sélectionné, si rien, on met la date du jour
if (isset($_GET['day']) && !empty($_GET['day'])){
    try {
        $date = new DateTime($_GET['day']);
    } catch (Exception $e) {
        header('location: index.php');
    }
}else{
    $date = new DateTime();
}
$day_before = (clone $date)->modify('-1 day')->format('Ymd');
$day_after = (clone $date)->modify('+1 day')->format('Ymd');

$hours = [
        '9h' => $date->format('Y-m-d') . ' ' . '09:00:00',
        '9h30' => $date->format('Y-m-d') . ' ' . '09:30:00',
        '10h' => $date->format('Y-m-d') . ' ' . '10:00:00',
        '10h30' => $date->format('Y-m-d') . ' ' . '10:30:00',
        '11h' => $date->format('Y-m-d') . ' ' . '11:00:00',
        '11h30' => $date->format('Y-m-d') . ' ' . '11:30:00',
        '12h' => $date->format('Y-m-d') . ' ' . '12:00:00',
        '12h30' => $date->format('Y-m-d') . ' ' . '12:30:00',
        '13h' => $date->format('Y-m-d') . ' ' . '13:00:00',
        '13h30' => $date->format('Y-m-d') . ' ' . '13:30:00',
        '14h' => $date->format('Y-m-d') . ' ' . '14:00:00',
        '14h30' => $date->format('Y-m-d') . ' ' . '14:30:00',
        '15h' => $date->format('Y-m-d') . ' ' . '15:00:00',
        '15h30' => $date->format('Y-m-d') . ' ' . '15:30:00',
        '16h' => $date->format('Y-m-d') . ' ' . '16:00:00',
        '16h30' => $date->format('Y-m-d') . ' ' . '16:30:00',
        '17h' => $date->format('Y-m-d') . ' ' . '17:00:00',
        '17h30' => $date->format('Y-m-d') . ' ' . '17:30:00',
        '18h' => $date->format('Y-m-d') . ' ' . '18:00:00',
        '18h30' => $date->format('Y-m-d') . ' ' . '18:30:00',
        '19h' => $date->format('Y-m-d') . ' ' . '19:30:00',
];

    // récupération des interventions journalières
$inters = $conn->prepare('SELECT id_employe, customer.name as csutomerName, customer.firstname as customerFirstname, date_debut, date_fin, num_inter, description FROM `intervention` INNER JOIN customer ON intervention.id_client=customer.id WHERE DATE(date_debut) = :date');
    $inters->execute([
        'date' => $date->format('Y-m-d'),
    ]);
    $inters = $inters->fetchAll();

    if(isset($_GET['inter']) && !empty($_GET['inter'])){
        $visu = $conn->prepare('SELECT employe.prenom, employe.nom, employe.id as id_employe, intervention.date_debut, intervention.id_client, intervention.id as id_inter, intervention.date_fin, intervention.num_inter, intervention.description, customer.firstname, customer.name FROM intervention INNER JOIN customer ON customer.id=id_client INNER JOIN employe ON employe.id=intervention.id_employe WHERE intervention.num_inter = :inter');
        $visu->execute([
           'inter' => $_GET['inter'],
        ]);
        $visu = $visu->fetch();
    }

    // récupération de la liste des employées en acitvité
$staff = $conn->prepare('SELECT * FROM employe WHERE date_sortie IS NULL');
$staff->execute();
$staff = $staff->fetchAll();
require 'header.php';
?>
<style>
    .table-block-active{
        width: auto;
        <?= count($staff) < 10 ? 'width: ' . 90 / count($staff) . '%' : ''; ?>;
    }
    td{
        height: 30px;
        text-align: center;
    }
</style>
<main class="mx-sm-3 my-3">
    <div class="d-flex justify-content-around mb-3">
        <h2>Liste des interventions du jour</h2>
        <h4><a href="/admin/interventions.php?day=<?= $day_before ?>" class="btn btn-info">&lt;</a><strong>&nbsp;<?= $date->format('d/m/Y') ?></strong>&nbsp;&nbsp;<a href="/admin/interventions.php?day=<?= $day_after ?>" class="btn btn-info">&gt;</a></h4>
        <a href="customers.php?inter=true" class="btn btn-success">Programmer une nouvelle intervention</a>
        <p>&nbsp;</p>
    </div>
    <table class="calendar__table table table-striped">
        <thead>
        <tr>
            <th></th>
            <?php foreach($staff as $staf):  ?>
                <th scope="col"><h5><strong><?= $staf['prenom'] . ' ' . $staf['nom'] ?></strong></h5><a href="addInter.php?tech=<?= $staf['id'] ?>&visu">Voir la semaine</a></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach($hours as $hour => $time): ?>
            <tr>
                <th scope="row" class="text-center border-hour"><?= $hour ?></th>
            <?php foreach ($staff as $staf): ?>
                <td class="table-block-active">
                <?php foreach($inters as $inter): ?>
                    <?= $inter['date_debut'] === $time && $inter['id_employe'] === $staf['id'] ? '<a href="/admin/interventions.php?inter=' . $inter['num_inter'] . '&day=' . $date->format('Ymd') . '" class="dateDebut">' . $inter['customerFirstname'] . ' ' . $inter['csutomerName'] . '</a>' : '' ;?>
                    <?= $inter['date_fin'] === $time && $inter['id_employe'] === $staf['id'] ? '<a href="/admin/interventions.php?inter=' . $inter['num_inter'] . '&day=' . $date->format('Ymd') . '" class="dateFin">' . substr($inter['description'], 0, 45) . '</a>' : '' ; ?>
                    <?= $inter['date_debut'] < $time && $inter['date_fin'] > $time && $inter['id_employe'] === $staf['id'] ? '<a href="/admin/interventions.php?inter=' . $inter['num_inter'] . '&day=' . $date->format('Ymd') . '" class="dateIntermed"><h1>?</h1></a>' : ''; ?>
                <?php endforeach; ?>
                </td>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>


    <!--Création de la popup lorsque la plage horaire est définie pour rentrer les dernières informations-->
    <?php
    if ((isset($_GET['inter']) && !empty($_GET['inter'])
            && isset($_POST['dateFin']) && !empty($_POST['dateFin']))
        || (isset($_GET['inter']) && !empty($_GET['inter']))){
        ?>
        <div class="modal fade" id="modalCalendar" data-show="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content align-items-center">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Visu de l'intervention<br><br>Technicien: <strong><?= $visu['prenom'] . ' ' . $visu['nom'] ?></strong></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body w-75 text-center">
                        <p>Date de début d'inter :<strong><?= date('Y-m-d H:m', strtotime($visu['date_debut'])) ?></strong></p>
                        <p>Date de fin d'inter :<strong><?= date('Y-m-d H:m', strtotime($visu['date_fin'])) ?></strong></p>
                    <table class="m-auto w-100">
                        <tbody class="text-center">
                        <tr class="d-flex flex-column align-items-center">
                            <td><strong>Nom du client:</strong></td>
                            <td class="w-100"><?= $visu['firstname'] . ' ' . $visu['name'] ?></td>
                                </tr>
                                <tr class="d-flex flex-column align-items-center">
                            <td class="mt-3"><strong>Description de l'intervention:</strong></td>
                            <td class="w-100"><?= $visu['description'] ?></td>
                        </tr>
                        <br>
                        <tr class="d-flex flex-column align-items-center mt-3">
                            <td><strong>Référence de l'intervention</strong></td>
                            <td class="my-2"><span class="ref_suivi"><?= $visu['num_inter'] ?></span></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="/admin/addInter.php?tech=<?= $visu['id_employe'] ?>&inter=<?= $visu['id_inter'] ?>&client=<?= $visu['id_client'] ?>" class="btn btn-outline-primary mt-3">Modification de la fiche</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</main>
<?php require '../footer.php';?>

