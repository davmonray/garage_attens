<?php

// Test pour savoir si un utilisateur est connecté
// On redirige vers la page login si aucun ne l'est

if (session_status() === PHP_SESSION_NONE){session_start();}
if ($_SESSION['role'] != 'admin'){header('location: /login.php');}

// Fin du test

require 'header.php';
?>
<main>
    <h1>Gestion et création de Fiches</h1>
</main>
<?php require '/footer.php';?>