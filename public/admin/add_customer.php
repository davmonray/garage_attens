<?php

require_once '../../libs/BDD/__connect.php';
if (isset($_POST['name']) && !empty($_POST['name'])
    && isset($_POST['firstname']) && !empty($_POST['firstname'])
    && isset($_POST['brand']) && !empty($_POST['brand'])
    && isset($_POST['type']) && !empty($_POST['type'])
    && isset($_POST['registration']) && !empty($_POST['registration'])
    && isset($_POST['tel']) && !empty($_POST['tel'])){

    // Create
    $create = $conn->prepare('INSERT INTO customer (name, firstname, brand, type, registration, tel, email) VALUE (:name, :firstname, :brand, :type, :registration, :tel, :email)');
    $create->execute([
        'name' => $_POST['name'],
        'firstname' => $_POST['firstname'],
        'brand' => $_POST['brand'],
        'type' => $_POST['type'],
        'registration' => $_POST['registration'],
        'tel' => $_POST['tel'],
        'email' => $_POST['email'],
    ]);
    $client_id = $conn->prepare('SELECT id FROM customer WHERE registration=:registration AND email=:email');
    $client_id->execute([
        'registration' => $_POST['registration'],
        'email' => $_POST['email'],
    ]);
    $client_id = $client_id->fetch();
    isset($_GET['prog']) && $_GET['prog'] ? header('location: addInter.php?client=' . $client_id['id']) : header('location: customers.php');
}

?>
<?php require 'header.php'; ?>
<main class="container">
    <h1>Ajout client</h1>
    <form class="d-flex flex-column align-items-center" method="POST" action="add_customer.php">
        <div class="form-group col-md-6">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="form-group col-md-6">
            <label for="firstname">Prénom</label>
            <input type="text" class="form-control" id="firstname" name="firstname" required>
        </div>
        <div class="form-group col-md-6">
            <label for="marque">Marque du véhicule</label>
            <input type="text" class="form-control" id="marque" name="brand" required>
        </div>
        <div class="form-group col-md-6">
            <label for="type">Type du véhicule</label>
            <input type="text" class="form-control" id="type" name="type" required>
        </div>
        <div class="form-group col-md-6">
            <label for="immat">Immatriculation</label>
            <input type="text" class="form-control" id="immat" name="registration" required>
        </div>
        <div class="form-group col-md-6">
            <label for="tel">Téléphone</label>
            <input type="text" class="form-control" id="tel" name="tel" required>
        </div>
        <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="form-group col-md-6">
            <input type="submit" class="btn btn-primary" value="Enregistrer">
        </div>
    </form>
    <br><br><br>
</main>
