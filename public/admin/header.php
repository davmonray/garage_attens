<?php
    require_once '../../vendor/autoload.php'
?><!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/style.css">
    <?= isset($css) ? '<link rel="stylesheet" href="'.$css.'">':""; ?>
    <title>Garage Charles et John Attens</title>
</head>
<body>
<header>
<nav class="navbar navbar-expand-lg navbar-dark bg-footer">
  <a class="navbar-brand" href="/">&nbsp;&nbsp;<img src="/assets/img/logo.png" width="70%"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php if (isset($sessionData) && $sessionData['roles'] == 'ROLE_USER'): ?>
      <li class="nav-item <?= isset($nav) && $nav === 'home' ? 'active':''; ?>">
        <a class="nav-link" href="/admin">Interventions du jour<span class="sr-only">(current)</span></a>
      </li>
        <?php endif ?>
      <li class="nav-item <?= isset($nav) && $nav === 'staff' ? 'active':''; ?>">
        <a class="nav-link" href="/admin/staff.php">Fiches personnel</a>
      </li>
      <li class="nav-item <?= isset($nav) && $nav === 'inters' ? 'active':''; ?>">
        <a class="nav-link" href="/admin/interventions.php">Gestion des interventions</a>
      </li>
      <li class="nav-item <?= isset($nav) && $nav === 'customers' ? 'active':''; ?>">
        <a class="nav-link" href="/admin/customers.php">Gestion des clients</a>
      </li>
        <?php if (isset($sessionData) && $sessionData['roles'] == 'ROLE_ADMIN'){ ?>
            <li class="nav-item <?= isset($nav) && $nav === 'contact' ? 'active':''; ?>">
                <a class="nav-link" href="/admin/Contact.php">Gestion des utilisateurs</a>
            </li>
        <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="/admin/logout.php">Logout</a>
      </li>
    </ul>
  </div>
</nav>
</header>