<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="./assets/style.css">
    <title><?php if($title != ""){echo $title;}else{echo 'Garage Charles et John Attens';}?></title>
</head>
<body>
<header>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="/">&nbsp;&nbsp;<img src="./assets/img/logo-dark.png" width="90%"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav">
      <li class="nav-item <?php if ($nav === 'index'):?>active<?php endif; ?>">
        <a class="nav-link" href="/">Accueil <span class="sr-only">(current)</span></a>
      </li>
        <li class="nav-item <?php if ($nav === 'login'):?>active<?php endif; ?>">
            <a class="nav-link" href="/login.php">Espace staff</a>
        </li>
      <li class="nav-item">
        <a class="nav-link link-rdv  <?php if ($nav === 'inters'):?>active<?php endif; ?>" href="/inters.php">Où en est mon RDV ?</a>
      </li>
    </ul>
  </div>
</nav>
</header>