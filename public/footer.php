  <footer class="bg-footer">
    <div class="container p-4">
      <div class="row">
        <div class="col-lg-6 col-md-12 mb-4">
          <h5 class="mb-3 text-white">Garage Charles Attens</h5>
          <p class="text-white">
              La SA Garage Charles et John Attens, est une entreprise de réparation automobile implantée à La Charte sur le Loir depuis 1997.<br><br>Toute intervention est garantie à vie selon la norme EX412-VM
          </p>
        </div>
        <div class="col-lg-3 col-md-6 mb-4">
          <h5 class="mb-3 text-white">Liens</h5>
          <ul class="list-unstyled mb-0">
            <li class="mb-1">
              <a href="/" class="text-white">Accueil</a>
            </li>
            <li class="mb-1">
              <a href="login.php" class="text-white">Connection</a>
            </li>
            <li class="mb-1">
              <a href="inters.php" class="text-white">Mon suivi d'intervention</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-6 mb-4">
          <h5 class="mb-1 text-white">Horaires</h5>
          <table class="table" style="border-color: #fff;">
            <tbody class="text-white">
              <tr>
                <td>Mardi au Samedi:<br>de 9h à 19h</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="text-center p-3 text-white" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2021 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">Garage C&J Attens</a>
    </div>
  </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
          $('#modalCalendar').modal('show');
      });
      $('.dateDebut').parents('td').addClass('dateDebut2');
      $('.dateFin').parents('td').addClass('dateFin2');
      $('.dateIntermed').parents('td').addClass('dateIntermed2');
      $('.inactive-case-calendar').parents('td').addClass('inactive-td-calendar');
    </script>
</body>
</html>