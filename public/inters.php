<?php
$title = 'suivi des interventions';
$nav = 'inters';
isset($conn) ? "" : require '../libs/BDD/__connect.php';
if(isset($_POST['inter']) && !empty($_POST['inter'])){
    $suivi = $conn->prepare('SELECT * FROM intervention INNER JOIN customer ON id_client = customer.id WHERE num_inter = :num');
    $suivi->execute([
        'num' => htmlspecialchars($_POST['inter']),
    ]);
    $suivi = $suivi->fetch();
    $suivi ? '': header('location: inters.php');
}
require './header.php';
?>
<style>body{height:100%}main{display:-ms-flexbox;display:-webkit-box;display:flex;-ms-flex-align:center;-ms-flex-pack:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:center;justify-content:center;padding-top:40px;padding-bottom:40px;background-color:#f5f5f5}.form-signin{width:100%;max-width:330px;padding:15px;margin:0 auto}.form-signin .checkbox{font-weight:400}.form-signin .form-control{position:relative;box-sizing:border-box;height:auto;padding:10px;font-size:16px}.form-signin .form-control:focus{z-index:2}.form-signin input[type=email]{margin-bottom:-1px;border-bottom-right-radius:0;border-bottom-left-radius:0}.form-signin input[type=password]{margin-bottom:10px;border-top-left-radius:0;border-top-right-radius:0}</style>
    <main class="text-center">
    <form class="form-signin" action="inters.php" method="POST">
          <img class="mb-4" src="./assets/img/inters.png" alt="" width="90" height="90">
          <h1 class="h3 mb-3 font-weight-normal">Suivi d'intervention</h1>
          <input type="text" name="inter" class="form-control" placeholder="Numéro d'intervention" required autofocus>
          <br>
          <input class="btn btn-lg btn-primary btn-block" type="submit" value="Rechercher">
        </form>
    </main>
<?php if(isset($suivi) && !empty($suivi)): ?>
    <div class="modal fade" id="modalCalendar" data-show="true" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">intervention pour le suivi <strong><?= $suivi['num_inter'] ?></strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    L'intervention pour le client: <strong><?= $suivi['firstname'] . ' ' . $suivi['name'] ?></strong><br><br>
                    Est prévue pour le <strong><?= date('d-m-Y', strtotime($suivi['date_debut'])) . ' à ' . date('H:i', strtotime($suivi['date_debut'])) ; ?></strong>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php require './footer.php';?>