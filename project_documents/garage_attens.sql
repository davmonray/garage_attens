-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : sam. 28 août 2021 à 17:19
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `garage_attens`
--

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `registration` varchar(14) NOT NULL,
  `tel` varchar(14) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `comments` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `customer`
--

INSERT INTO `customer` (`id`, `name`, `firstname`, `brand`, `type`, `registration`, `tel`, `email`, `comments`) VALUES
(1, 'divi', 'Remond', 'renault', 'laguna', 'bp-379-hl', '0604773577', 'bigreen@hotmail.fr', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `id` int(11) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `CP` char(5) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` varchar(14) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `num_secu` char(15) DEFAULT NULL,
  `date_entree` date NOT NULL,
  `date_sortie` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`id`, `prenom`, `nom`, `adresse`, `CP`, `ville`, `telephone`, `mail`, `num_secu`, `date_entree`, `date_sortie`, `id_user`) VALUES
(1, 'Virginie', 'Andurand', '2 route de joué l\'Abbé', '72460', 'JOUE L\'ABBE', '0631626436', 'v.andurand@laposte.net', '286083726118589', '2021-01-04', NULL, 16),
(4, 'tibo', 'lan', '51 rue drfZGFZ', '72000', 'LE MANS', '05151', 'remdavid2000@gmail.com', '18215485', '2021-05-11', '2021-08-17', NULL),
(5, 'vjekrnvb', 'zdves', 'verv', 'rve', 'verve', 'vrev', '', 'ervervre', '2021-06-08', NULL, NULL),
(7, 'Remond', 'divi', 'la Beaugendrie', '72130', 'Souge le Ganelon', '0604773577', 'remdavid2000@gmail.com', '17956324598', '2021-07-27', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE `intervention` (
  `id` int(11) NOT NULL,
  `id_employe` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `num_inter` int(11) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `intervention`
--

INSERT INTO `intervention` (`id`, `id_employe`, `id_client`, `date_debut`, `date_fin`, `num_inter`, `description`) VALUES
(5, 1, 1, '2021-08-10 09:00:00', '2021-08-10 10:30:00', 1628778885, 'sdfhsfhfh'),
(6, 1, 1, '2021-08-12 09:00:00', '2021-08-12 11:00:00', 1628781152, 'Démarrage en cote'),
(7, 5, 1, '2021-08-11 09:30:00', '2021-08-11 10:30:00', 1628781194, 'Walla'),
(8, 4, 1, '2021-08-11 09:00:00', '2021-08-11 11:00:00', 1628842102, 'qeryery'),
(9, 1, 1, '2021-08-17 12:00:00', '2021-08-17 15:30:00', 1629025220, 'nettoyage paumeau de vitesse avec un cure dent'),
(10, 1, 1, '2021-08-22 17:30:00', '2021-08-21 18:00:00', 1629293485, 'nettoyage du potot'),
(11, 1, 1, '2021-08-25 13:30:00', '2021-08-25 15:30:00', 1629890771, 'sfthsrt');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `roles` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `pwd`, `roles`) VALUES
(2, 'eleve', '$2y$10$TW7rE7f2SLqaXBdVzM8Ouu52xRrsX/R.Q5yEwkF.UZ0tSAHbjbHuG', 'ROLE_USER'),
(3, 'admin', '$2y$10$E0A08w8O8EZi7nCnOzE6fOSJJ2Sd28Ub.7ztPnJ3CpoAlAVamXgbS', 'ROLE_ADMIN'),
(7, 'test', '$2y$10$ioziMVXjltO5i44vfA.eHOkKw9PNK.nCOU8MeEgI/tIn/h/sxqMkS', 'Employé(e)'),
(16, 'ninie', '$2y$10$wMR72X7.nf2LgSzWHYjV7Ojp8xtsTeK9vXamyTuBg3TYBhkTBzRfe', 'ROLE_USER');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prenom` (`prenom`,`nom`,`num_secu`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_employe` (`id_employe`,`date_debut`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username_uindex` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `intervention`
--
ALTER TABLE `intervention`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `employe`
--
ALTER TABLE `employe`
  ADD CONSTRAINT `employe_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD CONSTRAINT `intervention_ibfk_1` FOREIGN KEY (`id_employe`) REFERENCES `employe` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
